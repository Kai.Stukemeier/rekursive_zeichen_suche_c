// Datei: suchen_rekursiv_main.cpp

#define CATCH_CONFIG_RUNNER
#include "catch.h"

#include <iostream>
using namespace std;

#include "suchen_rekursiv.h"

int main()
{
    Catch::Session().run();
	
    std::string text;
    std::string suchend;
    std::cout << "Bitte geben Sie den Text ein:";
    std::getline(std::cin, text);
    std::cout << "Bitte geben Sie die zu suchende Zeichenkette ein:";
    std::getline(std::cin, suchend);
    int output = zeichenkette_suchen_rekursiv(text, suchend);
    if (output == -1)
    {
        std::cout << "Die Zeichenkette '" << suchend << "' ist NICHT in dem Text '" << text << "' enthalten." << std::endl;
    }
    else
    {
        std::cout << "Die Zeichenkette '" << suchend << "' ist in dem Text '" << text << "' enthalten. \nSie startet ab Zeichen " << output << " (bei Zaehlung ab 0)." << std::endl;
    }

    system("PAUSE");
    return 0;
}