#include <string>
#include <iostream>
int zeichenkette_suchen_rekursiv(std::string text, std::string zkette, size_t text_pos = 0, size_t text_such_pos = 0, size_t zkette_such_pos = 0)
{
    int test = text.length();
    if (text_pos == text.length() and zkette_such_pos != zkette.length() or text_such_pos == text.length() and zkette_such_pos != zkette.length())
    {
        return -1;
    }
    else if (zkette_such_pos == zkette.length())
    {
        return text_pos;
    }

    if (text.at(text_pos) == zkette.at(zkette_such_pos) and text_such_pos == 0)
    {
        return zeichenkette_suchen_rekursiv(text, zkette, text_pos, text_pos + 1, zkette_such_pos + 1);
    }
    else if (text.at(text_pos) != zkette.at(zkette_such_pos) and text_such_pos == 0)
    {
        return zeichenkette_suchen_rekursiv(text, zkette, text_pos + 1, text_such_pos, zkette_such_pos);
    }
    else if (text.at(text_such_pos) == zkette.at(zkette_such_pos) and text_such_pos > 0)
    {
        return zeichenkette_suchen_rekursiv(text, zkette, text_pos, text_such_pos + 1, zkette_such_pos + 1);
    }
    else if (text.at(text_such_pos) != zkette.at(zkette_such_pos) and text_such_pos > 0)
    {
        return zeichenkette_suchen_rekursiv(text, zkette, text_such_pos + 1, 0, 0);
    }
    return 1;
}